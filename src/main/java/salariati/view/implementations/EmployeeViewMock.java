package salariati.view.implementations;

import salariati.controller.interfaces.EmployeeControllerInterface;
import salariati.enumeration.DidacticFunction;
import salariati.exception.EmployeeException;
import salariati.model.Employee;
import salariati.validator.EmployeeValidator;
import salariati.view.interfaces.EmployeeViewInterface;

import java.io.BufferedReader;
import java.io.Console;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Scanner;

public class EmployeeViewMock implements EmployeeViewInterface {
    private final EmployeeControllerInterface ctrl;

    public EmployeeViewMock(EmployeeControllerInterface ctrl) {
        this.ctrl = ctrl;
    }


    private void printMainMenu(){
        System.out.println("----- Meniul principal -----");
        System.out.println("1. Afiseaza angajatii");
        System.out.println("2. Adauga angajat nou");
        System.out.println("3. Modificare angajat");
        System.out.println("4. Sortare descrescatoare dupa salar");
        System.out.println("5. Sortare dupa cnp");
        System.out.println("6. Iesire din program");
        System.out.println("Introduceti valoarea: ");
    }

    private void addNewEmployee(){
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.println("Insert firstname: ");
            String firstName = bufferedReader.readLine();
            System.out.println("Insert lastname: ");
            String lastName = bufferedReader.readLine();
            System.out.println("Insert cnp: ");
            String cnp = bufferedReader.readLine();
            System.out.println("Choose function ( 0 - Asistent; 1 - Lector; 2 - Conferentiar ; 3 - Profesor): ");
            String function = bufferedReader.readLine();
            System.out.println("Insert salary: ");
            String salary = bufferedReader.readLine();
            Employee employee1 = new Employee(firstName,lastName,cnp,getDidacticFunctionByValue(function),salary);
            this.ctrl.addEmployee(employee1);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    private DidacticFunction getDidacticFunctionByValue(String value){
        if(value.equals("0"))
            return DidacticFunction.ASISTENT;
        else if(value.equals("1"))
            return DidacticFunction.LECTURER;
        else if(value.equals("2"))
            return DidacticFunction.CONFERENTIAR;
        else
            return DidacticFunction.TEACHER;
    }

    private void runMenu(){
        while(true){
            printMainMenu();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
            try {
                String menuValue = bufferedReader.readLine();
                if(menuValue.equals("1")){
                        for(Employee _employee : this.ctrl.getEmployeesList())
                            System.out.println(_employee.toString());
                }else if(menuValue.equals("2")) {
                    addNewEmployee();
                }else if(menuValue.equals("3")){
                        updateEmployee();
                } else if(menuValue.equals("4")) {
                    filterBySalary();
                }else if(menuValue.equals("5")) {
                    filterByCNP();
                }
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    private void filterByCNP() {
        List<Employee> list = ctrl.getEmployeeByCriteria("CNP");
        printList(list);
    }

    private void printList(List<Employee> list) {
        for(Employee _employee : list)
            System.out.println(_employee.toString());
    }

    private void filterBySalary() {
        List<Employee> list = ctrl.getEmployeeByCriteria("Salary");
        printList(list);
    }

    private void updateEmployee() {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.println("Insert employee id you want to update: ");
            String index = bufferedReader.readLine();

            System.out.println("Insert firstname: ");
            String firstName = bufferedReader.readLine();
            System.out.println("Insert lastname: ");
            String lastName = bufferedReader.readLine();
            System.out.println("Insert cnp: ");
            String cnp = bufferedReader.readLine();
            System.out.println("Choose function ( 0 - Asistent; 1 - Lector; 2 - Conferentiar ; 3 - Profesor): ");
            String function = bufferedReader.readLine();
            System.out.println("Insert salary: ");
            String salary = bufferedReader.readLine();
            Employee employee1 = new Employee(firstName,lastName,cnp,getDidacticFunctionByValue(function),salary);


            try {
                Employee old = ctrl.getEmployee(Integer.parseInt(index));
                this.ctrl.modifyEmployee(old, employee1);
            } catch (EmployeeException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public void run() {
        runMenu();
    }
}
