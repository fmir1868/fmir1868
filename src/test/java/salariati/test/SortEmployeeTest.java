package salariati.test;

import org.junit.Before;
import org.junit.Test;
import salariati.controller.implementations.EmployeeControllerImpl;
import salariati.enumeration.DidacticFunction;
import salariati.exception.EmployeeException;
import salariati.model.Employee;
import salariati.repository.implementations.EmployeeRepositoryImpl;
import salariati.repository.interfaces.EmployeeRepositoryInterface;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static junit.framework.TestCase.assertFalse;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by florescumaria on 07/05/2018.
 */
public class SortEmployeeTest {
    private EmployeeRepositoryInterface employeeRepository;

    @Before
    public void setUp() {
        employeeRepository = new EmployeeRepositoryImpl();
        employeeRepository.emptyFile();
        addEmployees();
    }

    private void addEmployees() {
        employeeRepository.addEmployee(new Employee("Maria", "Florescu", "2970312125824", DidacticFunction.LECTURER, "2312"));
        employeeRepository.addEmployee(new Employee("s", "s", "2960312125824", DidacticFunction.LECTURER, "21"));
        employeeRepository.addEmployee(new Employee("s", "s", "2930312125824" , DidacticFunction.LECTURER, "2122"));
    }

    @Test
    public void testEmployeesSize(){
        assertTrue(employeeRepository.getEmployeeList().size() == 3);

    }

    @Test
    public void sortCNPTestCorrectInput() throws EmployeeException {

        List<Employee> expected = new ArrayList<Employee>();
        expected.add(new Employee("s", "s", "2930312125824" , DidacticFunction.LECTURER, "2122"));
        expected.add(new Employee("s", "s", "2960312125824", DidacticFunction.LECTURER, "21"));
        expected.add(new Employee("Maria", "Florescu", "2970312125824", DidacticFunction.LECTURER, "2312"));

        List<Employee> sorted = employeeRepository.getEmployeeByCriteria("CNP");
        for(int i =0 ; i < sorted.size(); i++){
            assertTrue(expected.get(i).equals(sorted.get(i)));
        }
    }

    @Test
    public void sortSalaryTestCorrectInput() throws EmployeeException {

        List<Employee> expected = new ArrayList<Employee>();
        expected.add(new Employee("Maria", "Florescu", "2970312125824", DidacticFunction.LECTURER, "2312"));
        expected.add(new Employee("s", "s", "2930312125824" , DidacticFunction.LECTURER, "2122"));
        expected.add(new Employee("s", "s", "2960312125824", DidacticFunction.LECTURER, "21"));


        List<Employee> sorted = employeeRepository.getEmployeeByCriteria("Salary");
        for(int i =0 ; i < sorted.size(); i++){
            assertTrue(expected.get(i).equals(sorted.get(i)));
        }
    }


    @Test
    public void sortCNPTestIncorrectInput() throws EmployeeException {

        List<Employee> expected = new ArrayList<Employee>();
        expected.add(new Employee("s", "s", "2930312125824" , DidacticFunction.LECTURER, "2122"));
        expected.add(new Employee("s", "s", "2960312125824", DidacticFunction.LECTURER, "21"));
        expected.add(new Employee("Maria", "Florescu", "2970312125824", DidacticFunction.LECTURER, "2312"));
        expected.add(new Employee("Maria", "Florescu", "2970ncfmf312125824", DidacticFunction.LECTURER, "2312"));

        employeeRepository.addEmployee(new Employee("Maria", "Florescu", "2970dd312125824", DidacticFunction.LECTURER, "2sdf312"));

        List<Employee> sorted = employeeRepository.getEmployeeByCriteria("CNP");
        assertNotEquals(expected.toArray(),sorted.toArray());
    }

    @Test
    public void sortSalaryTestIncorrectInput() throws EmployeeException {

        List<Employee> expected = new ArrayList<Employee>();
        expected.add(new Employee("s", "s", "2930312125824" , DidacticFunction.LECTURER, "2122"));
        expected.add(new Employee("s", "s", "2960312125824", DidacticFunction.LECTURER, "21"));
        expected.add(new Employee("Maria", "Florescu", "2970312125824", DidacticFunction.LECTURER, "2312"));
        expected.add(new Employee("Maria", "Florescu", "2970312125824", DidacticFunction.LECTURER, "2312"));

        employeeRepository.addEmployee(new Employee("Maria", "Florescu", "2970312125824", DidacticFunction.LECTURER, "2sdf312"));

        List<Employee> sorted = employeeRepository.getEmployeeByCriteria("Salary");
        assertNotEquals(expected.toArray(),sorted.toArray());
    }
}
