package salariati.controller.interfaces;

import salariati.enumeration.DidacticFunction;
import salariati.exception.EmployeeException;
import salariati.model.Employee;

import java.util.List;

public interface EmployeeControllerInterface {
    void addEmployee(Employee employee);

    List<Employee> getEmployeesList();

    void modifyEmployee(Employee oldEmployee, Employee newEmployee) throws EmployeeException;

    void deleteEmployee(Employee employee);

    Employee getEmployee(int id);


    List<Employee> getEmployeeByCriteria(String criteria);


    void modifyEmployee(Employee employee, DidacticFunction didacticFunction) throws EmployeeException;
}
