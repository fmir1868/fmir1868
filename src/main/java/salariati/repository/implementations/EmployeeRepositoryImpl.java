package salariati.repository.implementations;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import salariati.enumeration.DidacticFunction;
import salariati.exception.EmployeeException;

import salariati.model.Employee;

import salariati.repository.interfaces.EmployeeRepositoryInterface;
import salariati.validator.EmployeeValidator;

public class EmployeeRepositoryImpl implements EmployeeRepositoryInterface {
	
	private final String employeeDBFile = "src/employees.txt";
	private EmployeeValidator employeeValidator = new EmployeeValidator();

	@Override
	public boolean addEmployee(Employee employee) {
		//if( employeeValidator.isValid(employee) ) {
			BufferedWriter bw = null;
			try {
				bw = new BufferedWriter(new FileWriter(employeeDBFile, true));
				bw.write(employee.toString());
				bw.newLine();
				bw.close();
				return true;
			} catch (IOException e) {
				e.printStackTrace();
			}
		//}
		return false;
	}

	@Override
	public void deleteEmployee(Employee employee) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void modifyEmployee(Employee oldEmployee, Employee newEmployee) throws EmployeeException{
		List<Employee> employees = getEmployeeList();

		for(Employee employee : employees){
			if(employee.equals(oldEmployee)){
			/*	employee.setLastName(newEmployee.getLastName());
				employee.setFirstName(newEmployee.getFirstName());
				employee.setCnp(newEmployee.getCnp());
				employee.setSalary(newEmployee.getSalary());*/
				employee.setFunction(newEmployee.getFunction());

			}
		}

		WriteInFile(employees);
		
	}


	public void modifyPosition(Employee oldEmployee, DidacticFunction didacticFunction) throws EmployeeException{
		List<Employee> employees = getEmployeeList();
		for(Employee employee : employees){
			if(employee.equals(oldEmployee)){
				employee.setFunction(didacticFunction);

			}
		}
		WriteInFile(employees);

	}

	public void modifyFunction(Employee oldEmployee, DidacticFunction didacticFunction) throws EmployeeException{
		List<Employee> employees = getEmployeeList();
		int i = 0;
		while(i < employees.size()){
			if(employees.get(i).equals(oldEmployee)){
				employees.get(i).setFunction(didacticFunction);

			}
			i++;
		}
		WriteInFile(employees);
	}

	private void WriteInFile(List<Employee> employees) {
		BufferedWriter bw = null;
		try {
			bw = new BufferedWriter(new FileWriter(employeeDBFile, false));
			bw.write("");
			bw.close();

		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			bw = new BufferedWriter(new FileWriter(employeeDBFile, true));
			for (Employee e: employees) {
				bw.write(e.toString());
				bw.newLine();
			}
			bw.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	public void emptyFile(){
		BufferedWriter bw = null;
		try {
			bw = new BufferedWriter(new FileWriter(employeeDBFile, false));
			bw.write("");
			bw.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	@Override
	public List<Employee> getEmployeeList() {
		List<Employee> employeeList = new ArrayList<Employee>();

		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(employeeDBFile));
			String line;
			int counter = 0;
			while ((line = br.readLine()) != null) {
				Employee employee = new Employee();
				try {
					employee = Employee.getEmployeeFromString(line, counter);
					employeeList.add(employee);
				} catch(EmployeeException ex) {
					System.err.println("Error while reading: " + ex.toString());
				}
			}
		} catch (FileNotFoundException e) {
			System.err.println("Error while reading: " + e);
		} catch (IOException e) {
			System.err.println("Error while reading: " + e);
		} finally {
			if (br != null)
				try {
					br.close();
				} catch (IOException e) {
					System.err.println("Error while closing the file: " + e);
				}
		}

		return employeeList;
	}


	@Override
	public List<Employee> getEmployeeByCriteria(String criteria) {
		List<Employee> employeeList = getEmployeeList();

		if (criteria.equals("Salary")) {
			Collections.sort(employeeList, new Comparator<Employee>(){
				public int compare(Employee o1, Employee o2){
					if(o1.getSalary().equals(o2.getSalary()))
						return 0;
					return o2.getSalary().compareTo(o1.getSalary());
				}
			});
		} else {
			if (criteria.equals("CNP")) {
				Collections.sort(employeeList, new Comparator<Employee>(){
					public int compare(Employee o1, Employee o2){
						String date1=o1.getCnp().substring(1,7);
						String date2=o2.getCnp().substring(1,7);
						if(date1.equals(date2)) {
							return 0;
						}
						return date1.compareTo(date2);
					}
				});
			}
		}

		return employeeList;
	}

	@Override
	public Employee getEmployee(int id) {
		List<Employee> employees = getEmployeeList();
		return employees.get(id -1);
	}

}
