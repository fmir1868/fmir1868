package salariati.test;

import org.junit.Before;
import org.junit.Test;
import salariati.controller.implementations.EmployeeControllerImpl;
import salariati.controller.interfaces.EmployeeControllerInterface;
import salariati.enumeration.DidacticFunction;
import salariati.exception.EmployeeException;
import salariati.model.Employee;
import salariati.repository.implementations.EmployeeRepositoryImpl;
import salariati.repository.interfaces.EmployeeRepositoryInterface;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;

/**
 * Created by florescumaria on 09/05/2018.
 */
public class IntegrationTestingTopDown {
    private EmployeeRepositoryInterface employeeRepository;
    private EmployeeControllerInterface controller;

    @Before
    public void setUp() {
        employeeRepository = new EmployeeRepositoryImpl();
        employeeRepository.emptyFile();
        controller = new EmployeeControllerImpl(employeeRepository);
    }

    @Test
    public void testunitA(){
        assertTrue(controller.getEmployeesList().size() == 0);
        Employee employee = new Employee("Pop","Mihai","192873937392", DidacticFunction.ASISTENT, "2345");
        controller.addEmployee(employee);
        assertTrue(controller.getEmployeesList().size() == 1);
    }


    @Test
    public void testB() throws EmployeeException {
        //A si B
        //P -> A -> B
        employeeRepository.emptyFile();
        Employee employee = new Employee("Pop","Mihai","192873937392", DidacticFunction.ASISTENT, "2345");
        controller.addEmployee(employee);
        assertTrue(controller.getEmployee(1).getFunction() == DidacticFunction.ASISTENT);
        controller.modifyEmployee(employee, DidacticFunction.TEACHER);
        assertTrue(controller.getEmployee(1).getFunction() == DidacticFunction.TEACHER);
    }

    @Test
    public void testC() throws EmployeeException {
        // A,B si C
        // P -> C -> B -> A
        employeeRepository.emptyFile();
        Employee employee = new Employee("Pop","Mihai","194873937392", DidacticFunction.ASISTENT, "1345");
        controller.addEmployee(employee);
        assertTrue(controller.getEmployee(1).getFunction() == DidacticFunction.ASISTENT);
        controller.modifyEmployee(employee, DidacticFunction.TEACHER);
        assertTrue(controller.getEmployee(1).getFunction() == DidacticFunction.TEACHER);

        controller.addEmployee(new Employee("Pop", "Alex", "1928482747294", DidacticFunction.LECTURER, "4241"));
        controller.addEmployee(new Employee("Andrei", "Alina", "2978482747294", DidacticFunction.LECTURER, "2241"));

        List<Employee> expected = new ArrayList<Employee>();
        expected.add(new Employee("Pop", "Alex", "1928482747294", DidacticFunction.LECTURER, "4241"));
        expected.add(new Employee("Andrei", "Alina", "2978482747294", DidacticFunction.LECTURER, "2241"));
        expected.add(new Employee("Pop","Mihai","194873937392", DidacticFunction.TEACHER, "1345"));

        List<Employee> salaries = controller.getEmployeeByCriteria("Salary");
        for(int i =0 ; i < salaries.size(); i++){
            assertTrue(expected.get(i).equals(salaries.get(i)));
        }

        expected.clear();

        expected.add(new Employee("Pop", "Alex", "1928482747294", DidacticFunction.LECTURER, "4241"));
        expected.add(new Employee("Pop","Mihai","194873937392", DidacticFunction.TEACHER, "1345"));
        expected.add(new Employee("Andrei", "Alina", "2978482747294", DidacticFunction.LECTURER, "2241"));


        List<Employee> cnp = controller.getEmployeeByCriteria("CNP");
        for(int i =0 ; i < salaries.size(); i++){
            assertTrue(expected.get(i).equals(cnp.get(i)));
        }

    }
}
