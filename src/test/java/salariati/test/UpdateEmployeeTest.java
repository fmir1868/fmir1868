package salariati.test;

import org.junit.Before;
import org.junit.Test;
import salariati.controller.implementations.EmployeeControllerImpl;
import salariati.enumeration.DidacticFunction;
import salariati.exception.EmployeeException;
import salariati.model.Employee;
import salariati.repository.implementations.EmployeeRepositoryImpl;
import salariati.repository.interfaces.EmployeeRepositoryInterface;
import salariati.repository.mock.EmployeeRepositoryMock;
import salariati.validator.EmployeeValidator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by florescumaria on 07/05/2018.
 */
public class UpdateEmployeeTest {
    private EmployeeRepositoryInterface employeeRepository;
    private EmployeeControllerImpl controller;
    private EmployeeValidator employeeValidator;

    @Before
    public void setUp() {
        employeeRepository = new EmployeeRepositoryImpl();
        employeeRepository.emptyFile();
        employeeValidator  = new EmployeeValidator();
    }

    @Test
    public void emptyFile(){
        employeeRepository.emptyFile();
        assertTrue(employeeRepository.getEmployeeList().size() == 0);
    }

    @Test
    public void employeeAdded(){
        employeeRepository.addEmployee(new Employee("Maria", "Pop", "2970312125824", DidacticFunction.LECTURER, "2312"));
        assertTrue(employeeRepository.getEmployeeList().size() == 1);
    }

    @Test
    public void testUpdateEmployee(){
        employeeRepository.emptyFile();
        Employee employee = new Employee("Maria", "Florescu", "2970312125824", DidacticFunction.LECTURER, "2312");

        employeeRepository.addEmployee(employee);

        try {
            employeeRepository.modifyFunction(employee, DidacticFunction.ASISTENT);
        } catch (EmployeeException e) {
            e.printStackTrace();
        }

        assertEquals(employeeRepository.getEmployee(1).getFunction(), DidacticFunction.ASISTENT);

    }
    @Test
    public void testUpdateEmployeePo2(){
        employeeRepository.emptyFile();
        Employee employee = new Employee("Maria", "Florescu", "2970312125824", DidacticFunction.LECTURER, "2312");

        try {
            employeeRepository.modifyFunction(employee, DidacticFunction.ASISTENT);
        } catch (EmployeeException e) {
            e.printStackTrace();
        }

        assertTrue(employeeRepository.getEmployeeList().size() == 0);

    }

    @Test
    public void testUpdateEmployeePo3(){
        employeeRepository.emptyFile();
        Employee employee = new Employee("Maria", "Florescu", "2970312125824", DidacticFunction.LECTURER, "2312");
        employeeRepository.addEmployee(employee);
        try {
            employeeRepository.modifyFunction(new Employee("Lar", "Florescu", "2970312125824", DidacticFunction.LECTURER, "2312"), DidacticFunction.TEACHER);
        } catch (EmployeeException e) {
            e.printStackTrace();
        }

        assertFalse(employeeRepository.getEmployee(1).getFunction().equals(DidacticFunction.TEACHER));

    }
}
