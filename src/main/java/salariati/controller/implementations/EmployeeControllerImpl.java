package salariati.controller.implementations;

import java.util.List;

import salariati.controller.interfaces.EmployeeControllerInterface;
import salariati.enumeration.DidacticFunction;
import salariati.exception.EmployeeException;
import salariati.model.Employee;
import salariati.repository.interfaces.EmployeeRepositoryInterface;

public class EmployeeControllerImpl implements EmployeeControllerInterface {
	
	private EmployeeRepositoryInterface employeeRepository;
	
	public EmployeeControllerImpl(EmployeeRepositoryInterface employeeRepository) {
		this.employeeRepository = employeeRepository;
	}
	
	@Override
	public void addEmployee(Employee employee) {
		employeeRepository.addEmployee(employee);
	}
	
	@Override
	public List<Employee> getEmployeesList() {
		return employeeRepository.getEmployeeList();
	}
	
	@Override
	public void modifyEmployee(Employee oldEmployee, Employee newEmployee) throws EmployeeException {
		employeeRepository.modifyEmployee(oldEmployee, newEmployee);
	}

	@Override
	public void deleteEmployee(Employee employee) {
		employeeRepository.deleteEmployee(employee);
	}

	@Override
	public Employee getEmployee(int id) {
		return employeeRepository.getEmployee(id);
	}

	@Override
	public List<Employee> getEmployeeByCriteria(String criteria) {
		return employeeRepository.getEmployeeByCriteria(criteria);
	}

	@Override
	public void modifyEmployee(Employee employee, DidacticFunction didacticFunction) throws EmployeeException {
		employeeRepository.modifyFunction(employee,didacticFunction);
	}

}
