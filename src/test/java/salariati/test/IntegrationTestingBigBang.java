package salariati.test;

import org.junit.Before;
import org.junit.Test;
import salariati.controller.implementations.EmployeeControllerImpl;
import salariati.controller.interfaces.EmployeeControllerInterface;
import salariati.enumeration.DidacticFunction;
import salariati.exception.EmployeeException;
import salariati.model.Employee;
import salariati.repository.implementations.EmployeeRepositoryImpl;
import salariati.repository.interfaces.EmployeeRepositoryInterface;
import salariati.validator.EmployeeValidator;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by florescumaria on 07/05/2018.
 */
public class IntegrationTestingBigBang {
    private EmployeeRepositoryInterface employeeRepository;
    private EmployeeControllerInterface controller;

    @Before
    public void setUp() {
        employeeRepository = new EmployeeRepositoryImpl();
        employeeRepository.emptyFile();
        controller = new EmployeeControllerImpl(employeeRepository);
    }

    @Test
    public void testUnitA(){
        Employee newEmployee = new Employee("Diana", "Popescu", "2910509055057", DidacticFunction.TEACHER, "9000");
        controller.addEmployee(newEmployee);
        assertEquals(1, controller.getEmployeesList().size());
    }

    @Test
    public void testUnitB() throws EmployeeException {
        employeeRepository.emptyFile();
        Employee newEmployee = new Employee("Maria", "bfdsrb", "2910509055057", DidacticFunction.TEACHER, "9000");
        controller.addEmployee(newEmployee);

        controller.modifyEmployee(newEmployee, DidacticFunction.ASISTENT);
        assertEquals(DidacticFunction.ASISTENT, controller.getEmployeesList().get(0).getFunction());
    }


    @Test
    public void testUnitC() throws EmployeeException {
        employeeRepository.emptyFile();
        Employee newEmployee = new Employee("Diana", "bfdsrb", "2940509055057", DidacticFunction.TEACHER, "9000");
        Employee newEmployee2 = new Employee("Diana", "bfdsrb", "2910509055057", DidacticFunction.TEACHER, "3000");
        Employee newEmployee3 = new Employee("Diana", "bfdsrb", "2990509055057", DidacticFunction.TEACHER, "5000");
        controller.addEmployee(newEmployee);
        controller.addEmployee(newEmployee2);
        controller.addEmployee(newEmployee3);


        List<Employee> expected = new ArrayList<Employee>();
        expected.add(new Employee("Diana", "bfdsrb", "2940509055057", DidacticFunction.TEACHER, "9000"));
        expected.add(new Employee("Diana", "bfdsrb", "2990509055057", DidacticFunction.TEACHER, "5000"));
        expected.add(new Employee("Diana", "bfdsrb", "2910509055057", DidacticFunction.TEACHER, "3000"));

        List<Employee> sorted = controller.getEmployeeByCriteria("Salary");


        for(int i =0 ; i < sorted.size(); i++){
            assertTrue(expected.get(i).equals(sorted.get(i)));
        }


        List<Employee> expected2 = new ArrayList<Employee>();
        expected2.add(new Employee("Diana", "bfdsrb", "2910509055057", DidacticFunction.TEACHER, "3000"));
        expected2.add(new Employee("Diana", "bfdsrb", "2940509055057", DidacticFunction.TEACHER, "9000"));
        expected2.add(new Employee("Diana", "bfdsrb", "2990509055057", DidacticFunction.TEACHER, "5000"));

        List<Employee> sorted2 = controller.getEmployeeByCriteria("CNP");


        for(int i =0 ; i < sorted2.size(); i++){
            assertTrue(expected2.get(i).equals(sorted2.get(i)));
        }
    }

    @Test
    public void integration() throws EmployeeException {
        employeeRepository.emptyFile();
        Employee newEmployee = new Employee("Diana", "bfdsrb", "2940509055057", DidacticFunction.TEACHER, "9000");
        Employee newEmployee2 = new Employee("Diana", "bfdsrb", "2910509055057", DidacticFunction.TEACHER, "3000");
        Employee newEmployee3 = new Employee("Diana", "bfdsrb", "2990509055057", DidacticFunction.TEACHER, "5000");
        controller.addEmployee(newEmployee);

        assertTrue(controller.getEmployee(1).getFirstName().equals("Diana"));
        assertTrue(controller.getEmployee(1).getLastName().equals("bfdsrb"));
        assertTrue(controller.getEmployee(1).getCnp().equals("2940509055057"));
        assertTrue(controller.getEmployee(1).getFunction().equals(DidacticFunction.TEACHER));
        assertTrue(controller.getEmployee(1).getSalary().equals("9000"));

        controller.modifyEmployee(newEmployee, DidacticFunction.ASISTENT);
        assertTrue(controller.getEmployee(1).getFunction().equals(DidacticFunction.ASISTENT));

        controller.addEmployee(newEmployee2);
        controller.addEmployee(newEmployee3);


        List<Employee> expected = new ArrayList<Employee>();
        expected.add(new Employee("Diana", "bfdsrb", "2940509055057", DidacticFunction.ASISTENT, "9000"));
        expected.add(new Employee("Diana", "bfdsrb", "2990509055057", DidacticFunction.TEACHER, "5000"));
        expected.add(new Employee("Diana", "bfdsrb", "2910509055057", DidacticFunction.TEACHER, "3000"));

        List<Employee> sorted = controller.getEmployeeByCriteria("Salary");


        for(int i =0 ; i < sorted.size(); i++){
            assertTrue(expected.get(i).equals(sorted.get(i)));
        }

    }



}
