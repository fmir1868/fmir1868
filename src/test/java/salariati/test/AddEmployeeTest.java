package salariati.test;

import static org.junit.Assert.*;

import salariati.exception.EmployeeException;
import salariati.model.Employee;

import org.junit.Before;
import org.junit.Test;

import salariati.repository.implementations.EmployeeRepositoryImpl;
import salariati.repository.interfaces.EmployeeRepositoryInterface;
import salariati.repository.mock.EmployeeRepositoryMock;
import salariati.validator.EmployeeValidator;
import salariati.controller.implementations.EmployeeControllerImpl;
import salariati.enumeration.DidacticFunction;

public class AddEmployeeTest {

	private EmployeeRepositoryInterface employeeRepository;
	
	@Before
	public void setUp() {
		employeeRepository = new EmployeeRepositoryImpl();
		employeeRepository.emptyFile();
	}
	
	@Test
	public void testRepositoryMock() {
		assertTrue(employeeRepository.getEmployeeList().isEmpty());
		assertEquals(0, employeeRepository.getEmployeeList().size());
	}
	
	@Test
	public void testAddNewEmployee() {
		Employee newEmployee = new Employee("Diana", "Popescu", "2910509055057", DidacticFunction.TEACHER, "9000");
		employeeRepository.addEmployee(newEmployee);
		assertEquals(1, employeeRepository.getEmployeeList().size());
		try {
			assertTrue(newEmployee.equals(employeeRepository.getEmployeeList().get(employeeRepository.getEmployeeList().size() - 1)));
		} catch (EmployeeException e) {
			e.printStackTrace();
		}
	}

}
