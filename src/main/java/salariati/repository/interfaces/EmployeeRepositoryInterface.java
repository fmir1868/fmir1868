package salariati.repository.interfaces;

import java.util.List;

import salariati.enumeration.DidacticFunction;
import salariati.exception.EmployeeException;
import salariati.model.Employee;

public interface EmployeeRepositoryInterface {
	
	boolean addEmployee(Employee employee);
	void deleteEmployee(Employee employee);
	void modifyEmployee(Employee oldEmployee, Employee newEmployee) throws EmployeeException;
	List<Employee> getEmployeeList();
	List<Employee> getEmployeeByCriteria(String criteria);
	Employee getEmployee(int id);
	void modifyFunction(Employee oldEmployee, DidacticFunction didacticFunction) throws EmployeeException;
	void emptyFile();

}
